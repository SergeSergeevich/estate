<?php
/* @var $this RealtyController */
/* @var $model Realty */

$this->breadcrumbs=array(
	'Realties'=>array('index'),
	$model->ID_realty,
);

$this->menu=array(
	array('label'=>'List Realty', 'url'=>array('index')),
	array('label'=>'Create Realty', 'url'=>array('create')),
	array('label'=>'Update Realty', 'url'=>array('update', 'id'=>$model->ID_realty)),
	array('label'=>'Delete Realty', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_realty),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Realty', 'url'=>array('admin')),
);
?>

<h1>View Realty #<?php echo $model->ID_realty; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_realty',
		'realty_type',
		'realty_type_defenition',
	),
)); ?>
