<?php
/* @var $this RealtyController */
/* @var $model Realty */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_realty'); ?>
		<?php echo $form->textField($model,'ID_realty'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realty_type'); ?>
		<?php echo $form->textField($model,'realty_type',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realty_type_defenition'); ?>
		<?php echo $form->textField($model,'realty_type_defenition',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->