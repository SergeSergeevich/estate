<?php
/* @var $this LightinfoController */
/* @var $model Lightinfo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_info'); ?>
		<?php echo $form->textField($model,'ID_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'short_annotation'); ?>
		<?php echo $form->textArea($model,'short_annotation',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_photo'); ?>
		<?php echo $form->textField($model,'ID_photo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_realty'); ?>
		<?php echo $form->textField($model,'ID_realty'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_coast'); ?>
		<?php echo $form->textField($model,'ID_coast'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_status'); ?>
		<?php echo $form->textField($model,'ID_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_country'); ?>
		<?php echo $form->textField($model,'ID_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cost'); ?>
		<?php echo $form->textField($model,'cost',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->