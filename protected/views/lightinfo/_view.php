<?php
/* @var $this LightinfoController */
/* @var $data Lightinfo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_info')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_info), array('view', 'id'=>$data->ID_info)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_annotation')); ?>:</b>
	<?php echo CHtml::encode($data->short_annotation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_photo')); ?>:</b>
	<?php echo CHtml::encode($data->ID_photo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_realty')); ?>:</b>
	<?php echo CHtml::encode($data->ID_realty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_coast')); ?>:</b>
	<?php echo CHtml::encode($data->ID_coast); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_status')); ?>:</b>
	<?php echo CHtml::encode($data->ID_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_country')); ?>:</b>
	<?php echo CHtml::encode($data->ID_country); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	*/ ?>

</div>