<?php
/* @var $this ShortController */
/* @var $data Short */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_info')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_info), array('view', 'id'=>$data->ID_info)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_offer')); ?>:</b>
	<?php echo CHtml::encode($data->ID_offer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_annotation')); ?>:</b>
	<?php echo CHtml::encode($data->short_annotation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_description')); ?>:</b>
	<?php echo CHtml::encode($data->short_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_photo')); ?>:</b>
	<?php echo CHtml::encode($data->ID_photo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_realty')); ?>:</b>
	<?php echo CHtml::encode($data->ID_realty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_coast')); ?>:</b>
	<?php echo CHtml::encode($data->ID_coast); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_status')); ?>:</b>
	<?php echo CHtml::encode($data->ID_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rental_description')); ?>:</b>
	<?php echo CHtml::encode($data->rental_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_country')); ?>:</b>
	<?php echo CHtml::encode($data->ID_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('build_year')); ?>:</b>
	<?php echo CHtml::encode($data->build_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_area')); ?>:</b>
	<?php echo CHtml::encode($data->full_area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('house_area')); ?>:</b>
	<?php echo CHtml::encode($data->house_area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terrace_area')); ?>:</b>
	<?php echo CHtml::encode($data->terrace_area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bedroom')); ?>:</b>
	<?php echo CHtml::encode($data->bedroom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bathroom')); ?>:</b>
	<?php echo CHtml::encode($data->bathroom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('furniture')); ?>:</b>
	<?php echo CHtml::encode($data->furniture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('swimming_pool')); ?>:</b>
	<?php echo CHtml::encode($data->swimming_pool); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fireplace')); ?>:</b>
	<?php echo CHtml::encode($data->fireplace); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distance_to_sea')); ?>:</b>
	<?php echo CHtml::encode($data->distance_to_sea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_per_month')); ?>:</b>
	<?php echo CHtml::encode($data->credit_per_month); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_description')); ?>:</b>
	<?php echo CHtml::encode($data->full_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coast_name')); ?>:</b>
	<?php echo CHtml::encode($data->coast_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('small')); ?>:</b>
	<?php echo CHtml::encode($data->small); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_type')); ?>:</b>
	<?php echo CHtml::encode($data->status_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realty_type')); ?>:</b>
	<?php echo CHtml::encode($data->realty_type); ?>
	<br />

	*/ ?>

</div>