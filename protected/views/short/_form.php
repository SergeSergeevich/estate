<?php
/* @var $this ShortController */
/* @var $model Short */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'short-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_info'); ?>
		<?php echo $form->textField($model,'ID_info'); ?>
		<?php echo $form->error($model,'ID_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_offer'); ?>
		<?php echo $form->textArea($model,'ID_offer',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ID_offer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short_annotation'); ?>
		<?php echo $form->textArea($model,'short_annotation',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'short_annotation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short_description'); ?>
		<?php echo $form->textArea($model,'short_description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'short_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_photo'); ?>
		<?php echo $form->textField($model,'ID_photo'); ?>
		<?php echo $form->error($model,'ID_photo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_realty'); ?>
		<?php echo $form->textField($model,'ID_realty'); ?>
		<?php echo $form->error($model,'ID_realty'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_coast'); ?>
		<?php echo $form->textField($model,'ID_coast'); ?>
		<?php echo $form->error($model,'ID_coast'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_status'); ?>
		<?php echo $form->textField($model,'ID_status'); ?>
		<?php echo $form->error($model,'ID_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rental_description'); ?>
		<?php echo $form->textArea($model,'rental_description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'rental_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_country'); ?>
		<?php echo $form->textField($model,'ID_country'); ?>
		<?php echo $form->error($model,'ID_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'build_year'); ?>
		<?php echo $form->textField($model,'build_year'); ?>
		<?php echo $form->error($model,'build_year'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'full_area'); ?>
		<?php echo $form->textField($model,'full_area',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'full_area'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'house_area'); ?>
		<?php echo $form->textField($model,'house_area',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'house_area'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'terrace_area'); ?>
		<?php echo $form->textField($model,'terrace_area',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'terrace_area'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bedroom'); ?>
		<?php echo $form->textField($model,'bedroom',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'bedroom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bathroom'); ?>
		<?php echo $form->textField($model,'bathroom',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'bathroom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'furniture'); ?>
		<?php echo $form->textArea($model,'furniture',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'furniture'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'swimming_pool'); ?>
		<?php echo $form->textField($model,'swimming_pool',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'swimming_pool'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fireplace'); ?>
		<?php echo $form->textField($model,'fireplace',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'fireplace'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'distance_to_sea'); ?>
		<?php echo $form->textField($model,'distance_to_sea',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'distance_to_sea'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'credit_per_month'); ?>
		<?php echo $form->textField($model,'credit_per_month',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'credit_per_month'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textArea($model,'location',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'full_description'); ?>
		<?php echo $form->textArea($model,'full_description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'full_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coast_name'); ?>
		<?php echo $form->textField($model,'coast_name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'coast_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'small'); ?>
		<?php echo $form->textField($model,'small',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'small'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_type'); ?>
		<?php echo $form->textField($model,'status_type',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'status_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'realty_type'); ?>
		<?php echo $form->textField($model,'realty_type',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'realty_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->