<?php
/* @var $this ShortController */
/* @var $model Short */

$this->breadcrumbs=array(
	'Shorts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Short', 'url'=>array('index')),
	array('label'=>'Manage Short', 'url'=>array('admin')),
);
?>

<h1>Create Short</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>