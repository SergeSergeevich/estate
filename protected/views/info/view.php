<?php
/* @var $this InfoController */
/* @var $model Info */

$this->breadcrumbs=array(
	'Infos'=>array('index'),
	$model->ID_info,
);

$this->menu=array(
	array('label'=>'List Info', 'url'=>array('index')),
	array('label'=>'Create Info', 'url'=>array('create')),
	array('label'=>'Update Info', 'url'=>array('update', 'id'=>$model->ID_info)),
	array('label'=>'Delete Info', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_info),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Info', 'url'=>array('admin')),
);
?>

<h1>View Info #<?php echo $model->ID_info; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_info',
		'ID_offer',
		'short_annotation',
		'short_description',
		'ID_photo',
		'ID_realty',
		'ID_coast',
		'ID_status',
		'rental_description',
		'ID_country',
		'cost',
		'build_year',
		'full_area',
		'house_area',
		'terrace_area',
		'bedroom',
		'bathroom',
		'furniture',
		'swimming_pool',
		'fireplace',
		'distance_to_sea',
		'credit_per_month',
		'address',
		'location',
		'comment',
		'full_description',
	),
)); ?>
