<?php
/* @var $this CoastController */
/* @var $model Coast */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_coast'); ?>
		<?php echo $form->textField($model,'ID_coast'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coast_name'); ?>
		<?php echo $form->textField($model,'coast_name',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->