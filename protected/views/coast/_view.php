<?php
/* @var $this CoastController */
/* @var $data Coast */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_coast')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_coast), array('view', 'id'=>$data->ID_coast)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coast_name')); ?>:</b>
	<?php echo CHtml::encode($data->coast_name); ?>
	<br />


</div>