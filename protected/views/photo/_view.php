<?php
/* @var $this PhotoController */
/* @var $data Photo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_photo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_photo), array('view', 'id'=>$data->ID_photo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commentary')); ?>:</b>
	<?php echo CHtml::encode($data->commentary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('photo_name')); ?>:</b>
	<?php echo CHtml::encode($data->photo_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('big')); ?>:</b>
	<?php echo CHtml::encode($data->big); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('small')); ?>:</b>
	<?php echo CHtml::encode($data->small); ?>
	<br />


</div>