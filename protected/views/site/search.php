<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form TbActiveForm */

$this->pageTitle=Yii::app()->name . ' - Search';
$this->breadcrumbs=array(
    'Search',
);
?>

<h1>Search form</h1>

<?php if(Yii::app()->user->hasFlash('search')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('search'),
    )); ?>

<?php else: ?>

<p>
Fill search form:
</p>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'search-form',
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model,'searchString'); ?>

    <?php //echo $form->textFieldRow($model,'textId'); ?>

    <?php //echo $form->textFieldRow($model,'number'); ?>

    <?php //echo $form->textFieldRow($model,'coastNumber'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Submit',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

<?php if (!empty($dataProvider)): ?>

    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'search-grid',
        'dataProvider' => $dataProvider,
        'filter'=>$infoModel,
//        'columns'=>array(
//            'lastName',
//            'firstName',
//            'email',
//        ),
    )); ?>


<?php endif; ?>


</div><!-- form -->

<?php endif; ?>