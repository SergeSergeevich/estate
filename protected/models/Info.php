<?php

/**
 * This is the model class for table "info".
 *
 * The followings are the available columns in table 'info':
 * @property integer $ID_info
 * @property string $ID_offer
 * @property string $short_annotation
 * @property string $short_description
 * @property integer $ID_photo
 * @property integer $ID_realty
 * @property integer $ID_coast
 * @property integer $ID_status
 * @property string $rental_description
 * @property integer $ID_country
 * @property string $cost
 * @property integer $build_year
 * @property string $full_area
 * @property string $house_area
 * @property string $terrace_area
 * @property string $bedroom
 * @property string $bathroom
 * @property string $furniture
 * @property string $swimming_pool
 * @property string $fireplace
 * @property string $distance_to_sea
 * @property string $credit_per_month
 * @property string $address
 * @property string $location
 * @property string $comment
 * @property string $full_description
 *
 * The followings are the available model relations:
 * @property Photo $iDPhoto
 * @property Realty $iDRealty
 * @property Coast $iDCoast
 * @property Status $iDStatus
 * @property Country $iDCountry
 */
class Info extends CActiveRecord
{
    public $image;
    public $Commentary_to_photo;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Info the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_photo, ID_realty, ID_coast, ID_status, ID_country, build_year', 'numerical', 'integerOnly'=>true),
			array('cost, full_area, house_area, terrace_area, distance_to_sea, credit_per_month', 'length', 'max'=>10),
			array('bedroom, bathroom, swimming_pool, fireplace', 'length', 'max'=>100),
			array('Commentary_to_photo, ID_offer, short_annotation, short_description, rental_description, furniture, address, location, comment, full_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_info, ID_offer, short_annotation, short_description, ID_photo, ID_realty, ID_coast, ID_status, rental_description, ID_country, cost, build_year, full_area, house_area, terrace_area, bedroom, bathroom, furniture, swimming_pool, fireplace, distance_to_sea, credit_per_month, address, location, comment, full_description', 'safe', 'on'=>'search'),
            array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty' => true),
           // array('image', 'required'),
           
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDPhoto' => array(self::BELONGS_TO, 'Photo', 'ID_photo'),
			'iDRealty' => array(self::BELONGS_TO, 'Realty', 'ID_realty'),
			'iDCoast' => array(self::BELONGS_TO, 'Coast', 'ID_coast'),
			'iDStatus' => array(self::BELONGS_TO, 'Status', 'ID_status'),
			'iDCountry' => array(self::BELONGS_TO, 'Country', 'ID_country'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_info' => 'Id Info',
			'ID_offer' => 'Id Offer',
			'short_annotation' => 'Short Annotation',
			'short_description' => 'Short Description',
			'ID_photo' => 'ID Photo',
			'ID_realty' => 'Type of realty',
			'ID_coast' => 'Choose the coast',
			'ID_status' => 'Type of status',
			'rental_description' => 'Rental Description',
			'ID_country' => 'Country',
			'cost' => 'Cost',
			'build_year' => 'Build Year',
			'full_area' => 'Full Area',
			'house_area' => 'House Area',
			'terrace_area' => 'Terrace Area',
			'bedroom' => 'Bedroom',
			'bathroom' => 'Bathroom',
			'furniture' => 'Furniture',
			'swimming_pool' => 'Swimming Pool',
			'fireplace' => 'Fireplace',
			'distance_to_sea' => 'Distance To Sea(or leave empty)',
			'credit_per_month' => 'Credit per month',
			'address' => 'Address',
			'location' => 'Location',
			'comment' => 'Comment',
			'full_description' => 'Full Description',
            'image' => 'Your preview photo',
		);
	}
    
    public function after(){
            //find realty_type_defenetion from the table Realty, according value user choosen
            $criteria=new CDbCriteria;
            $criteria->select='realty_type_defenition';
            $criteria->condition='ID_realty=:ID_realty';
            $criteria->params=array(':ID_realty'=>$this->ID_realty);
            $realty_defenition=Realty::model()->find($criteria);
            $realty_type = $realty_defenition->attributes['realty_type_defenition'];
            //$l = Yii::app()->db->getLastInsertId();
            //loading last saved model from db
            $last = Info::model()->findByPk($l);
            
            //getting ID_coast
            $coast = $this->ID_coast;
            //getting ID_info
            $real_def = $this->ID_info;
            //check if 'ID_info < 5', adding zeros before ID untill it's length size become 4  
            if(strlen($real_def) < 5){
                for($i=0;$i<strlen($real_def);$i++){
                    if(strlen($real_def)<4){
                        $real_def = '0'.$real_def;
                    }else break;
                }
            }
            //concat 3 variables to form ID_offer as said in docs
            $last->ID_offer = $realty_type . $real_def .'/'. $coast;
            //update dbs' last record
            $last->update();      
    }
    
    
    
   //Dropdown lists for Realty,Coast,Status,Country.Other part is in _form.php 
    public function getRealtyType(){
        return CHtml::listData(Realty::model()->findAll(), 'ID_realty', 'realty_type');
    }
    
    public function getCoast(){
        return CHtml::listData(Coast::model()->findAll(), 'ID_coast', 'coast_name');
    }
    
    public function getStatusType(){
        return CHtml::listData(Status::model()->findAll(), 'ID_status', 'status_type');
    }
    
    public function getCountries(){
        return CHtml::listData(Country::model()->findAll(), 'ID_country', 'country_name');
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_info',$this->ID_info);
		$criteria->compare('ID_offer',$this->ID_offer,true);
		$criteria->compare('short_annotation',$this->short_annotation,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('ID_photo',$this->ID_photo);
		$criteria->compare('ID_realty',$this->ID_realty);
		$criteria->compare('ID_coast',$this->ID_coast);
		$criteria->compare('ID_status',$this->ID_status);
		$criteria->compare('rental_description',$this->rental_description,true);
		$criteria->compare('ID_country',$this->ID_country);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('build_year',$this->build_year);
		$criteria->compare('full_area',$this->full_area,true);
		$criteria->compare('house_area',$this->house_area,true);
		$criteria->compare('terrace_area',$this->terrace_area,true);
		$criteria->compare('bedroom',$this->bedroom,true);
		$criteria->compare('bathroom',$this->bathroom,true);
		$criteria->compare('furniture',$this->furniture,true);
		$criteria->compare('swimming_pool',$this->swimming_pool,true);
		$criteria->compare('fireplace',$this->fireplace,true);
		$criteria->compare('distance_to_sea',$this->distance_to_sea,true);
		$criteria->compare('credit_per_month',$this->credit_per_month,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('full_description',$this->full_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}