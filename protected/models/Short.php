<?php

/**
 * This is the model class for table "short".
 *
 * The followings are the available columns in table 'short':
 * @property integer $ID_info
 * @property string $ID_offer
 * @property string $short_annotation
 * @property string $short_description
 * @property integer $ID_photo
 * @property integer $ID_realty
 * @property integer $ID_coast
 * @property integer $ID_status
 * @property string $rental_description
 * @property integer $ID_country
 * @property string $cost
 * @property integer $build_year
 * @property string $full_area
 * @property string $house_area
 * @property string $terrace_area
 * @property string $bedroom
 * @property string $bathroom
 * @property string $furniture
 * @property string $swimming_pool
 * @property string $fireplace
 * @property string $distance_to_sea
 * @property string $credit_per_month
 * @property string $address
 * @property string $location
 * @property string $comment
 * @property string $full_description
 * @property string $coast_name
 * @property string $small
 * @property string $status_type
 * @property string $realty_type
 */
class Short extends CActiveRecord
{
        public function primaryKey(){
            return 'ID_info';
        }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Short the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'short';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_info, ID_photo, ID_realty, ID_coast, ID_status, ID_country, build_year', 'numerical', 'integerOnly'=>true),
			array('cost, full_area, house_area, terrace_area, distance_to_sea, credit_per_month', 'length', 'max'=>10),
			array('bedroom, bathroom, swimming_pool, fireplace', 'length', 'max'=>100),
			array('coast_name, realty_type', 'length', 'max'=>20),
			array('small', 'length', 'max'=>30),
			array('status_type', 'length', 'max'=>15),
			array('ID_offer, short_annotation, short_description, rental_description, furniture, address, location, comment, full_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_info, ID_offer, short_annotation, short_description, ID_photo, ID_realty, ID_coast, ID_status, rental_description, ID_country, cost, build_year, full_area, house_area, terrace_area, bedroom, bathroom, furniture, swimming_pool, fireplace, distance_to_sea, credit_per_month, address, location, comment, full_description, coast_name, small, status_type, realty_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_info' => 'Id Info',
			'ID_offer' => 'Id Offer',
			'short_annotation' => 'Short Annotation',
			'short_description' => 'Short Description',
			'ID_photo' => 'Id Photo',
			'ID_realty' => 'Id Realty',
			'ID_coast' => 'Id Coast',
			'ID_status' => 'Id Status',
			'rental_description' => 'Rental Description',
			'ID_country' => 'Id Country',
			'cost' => 'Cost',
			'build_year' => 'Build Year',
			'full_area' => 'Full Area',
			'house_area' => 'House Area',
			'terrace_area' => 'Terrace Area',
			'bedroom' => 'Bedroom',
			'bathroom' => 'Bathroom',
			'furniture' => 'Furniture',
			'swimming_pool' => 'Swimming Pool',
			'fireplace' => 'Fireplace',
			'distance_to_sea' => 'Distance To Sea',
			'credit_per_month' => 'Credit Per Month',
			'address' => 'Address',
			'location' => 'Location',
			'comment' => 'Comment',
			'full_description' => 'Full Description',
			'coast_name' => 'Coast Name',
			'small' => 'Small',
			'status_type' => 'Status Type',
			'realty_type' => 'Realty Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_info',$this->ID_info);
		$criteria->compare('ID_offer',$this->ID_offer,true);
		$criteria->compare('short_annotation',$this->short_annotation,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('ID_photo',$this->ID_photo);
		$criteria->compare('ID_realty',$this->ID_realty);
		$criteria->compare('ID_coast',$this->ID_coast);
		$criteria->compare('ID_status',$this->ID_status);
		$criteria->compare('rental_description',$this->rental_description,true);
		$criteria->compare('ID_country',$this->ID_country);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('build_year',$this->build_year);
		$criteria->compare('full_area',$this->full_area,true);
		$criteria->compare('house_area',$this->house_area,true);
		$criteria->compare('terrace_area',$this->terrace_area,true);
		$criteria->compare('bedroom',$this->bedroom,true);
		$criteria->compare('bathroom',$this->bathroom,true);
		$criteria->compare('furniture',$this->furniture,true);
		$criteria->compare('swimming_pool',$this->swimming_pool,true);
		$criteria->compare('fireplace',$this->fireplace,true);
		$criteria->compare('distance_to_sea',$this->distance_to_sea,true);
		$criteria->compare('credit_per_month',$this->credit_per_month,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('full_description',$this->full_description,true);
		$criteria->compare('coast_name',$this->coast_name,true);
		$criteria->compare('small',$this->small,true);
		$criteria->compare('status_type',$this->status_type,true);
		$criteria->compare('realty_type',$this->realty_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}