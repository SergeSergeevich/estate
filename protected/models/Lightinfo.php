<?php

/**
 * This is the model class for table "lightinfo".
 *
 * The followings are the available columns in table 'lightinfo':
 * @property integer $ID_info
 * @property string $short_annotation
 * @property integer $ID_photo
 * @property integer $ID_realty
 * @property integer $ID_coast
 * @property integer $ID_status
 * @property integer $ID_country
 * @property string $cost
 */
class Lightinfo extends CActiveRecord
{
            public function primaryKey(){
            return 'ID_info';
        }
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Lightinfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lightinfo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_info, ID_photo, ID_realty, ID_coast, ID_status, ID_country', 'numerical', 'integerOnly'=>true),
			array('cost', 'length', 'max'=>10),
			array('short_annotation', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_info, short_annotation, ID_photo, ID_realty, ID_coast, ID_status, ID_country, cost', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_info' => 'Id Info',
			'short_annotation' => 'Short Annotation',
			'ID_photo' => 'Id Photo',
			'ID_realty' => 'Id Realty',
			'ID_coast' => 'Id Coast',
			'ID_status' => 'Id Status',
			'ID_country' => 'Id Country',
			'cost' => 'Cost',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_info',$this->ID_info);
		$criteria->compare('short_annotation',$this->short_annotation,true);
		$criteria->compare('ID_photo',$this->ID_photo);
		$criteria->compare('ID_realty',$this->ID_realty);
		$criteria->compare('ID_coast',$this->ID_coast);
		$criteria->compare('ID_status',$this->ID_status);
		$criteria->compare('ID_country',$this->ID_country);
		$criteria->compare('cost',$this->cost,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}