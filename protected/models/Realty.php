<?php

/**
 * This is the model class for table "realty".
 *
 * The followings are the available columns in table 'realty':
 * @property integer $ID_realty
 * @property string $realty_type
 * @property string $realty_type_defenition
 *
 * The followings are the available model relations:
 * @property Info[] $infos
 */
class Realty extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Realty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'realty';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('realty_type', 'length', 'max'=>20),
			array('realty_type_defenition', 'length', 'max'=>2),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_realty, realty_type, realty_type_defenition', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'infos' => array(self::HAS_MANY, 'Info', 'ID_realty'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_realty' => 'Id Realty',
			'realty_type' => 'Realty Type',
			'realty_type_defenition' => 'Realty Type Defenition',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_realty',$this->ID_realty);
		$criteria->compare('realty_type',$this->realty_type,true);
		$criteria->compare('realty_type_defenition',$this->realty_type_defenition,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}