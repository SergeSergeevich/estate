-- phpMyAdmin SQL Dump
-- version 2.11.9.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 21 2013 г., 16:50
-- Версия сервера: 5.0.67
-- Версия PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `catalogue_of_realty`
--

-- --------------------------------------------------------

--
-- Структура таблицы `coast`
--

CREATE TABLE IF NOT EXISTS `coast` (
  `ID_coast` int(11) NOT NULL auto_increment,
  `coast_name` varchar(20) default NULL,
  PRIMARY KEY  (`ID_coast`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `coast`
--

INSERT INTO `coast` (`ID_coast`, `coast_name`) VALUES
(1, 'North'),
(2, 'Northeast'),
(3, 'East'),
(4, 'Southeast'),
(5, 'South'),
(6, 'Southwest'),
(7, 'West'),
(8, 'Northwest');

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `ID_country` int(11) NOT NULL auto_increment,
  `country_name` varchar(20) default NULL,
  PRIMARY KEY  (`ID_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`ID_country`, `country_name`) VALUES
(1, 'Canada'),
(2, 'USA'),
(3, 'Mexico'),
(4, 'Cuba'),
(5, 'Guatemala'),
(6, 'Nicaragua'),
(7, 'Honduras'),
(8, 'Costa Rica'),
(9, 'Panama'),
(10, 'Colombia'),
(11, 'Venezuela'),
(12, 'Brasil'),
(13, 'Peru'),
(14, 'Ecuador'),
(15, 'Chile'),
(16, 'Uruguay'),
(17, 'Argentina'),
(18, 'Republica Dominicana');

-- --------------------------------------------------------

--
-- Структура таблицы `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `ID_info` int(11) NOT NULL auto_increment,
  `ID_offer` tinytext,
  `short_annotation` tinytext,
  `short_description` tinytext,
  `ID_photo` int(11) default NULL,
  `ID_realty` int(11) default NULL,
  `ID_coast` int(11) default NULL,
  `ID_status` int(11) default NULL,
  `rental_description` tinytext,
  `ID_country` int(11) default NULL,
  `cost` decimal(10,2) default NULL,
  `build_year` smallint(4) default NULL,
  `full_area` decimal(10,2) default NULL,
  `house_area` decimal(10,2) default NULL,
  `terrace_area` decimal(10,2) default NULL,
  `bedroom` varchar(100) default NULL,
  `bathroom` varchar(100) default NULL,
  `furniture` tinytext,
  `swimming_pool` varchar(100) default NULL,
  `fireplace` varchar(100) default NULL,
  `distance_to_sea` decimal(10,2) default NULL,
  `credit_per_month` decimal(10,2) default NULL,
  `address` tinytext,
  `location` tinytext,
  `comment` text,
  `full_description` text,
  PRIMARY KEY  (`ID_info`),
  KEY `ID_photo` (`ID_photo`),
  KEY `ID_realty` (`ID_realty`),
  KEY `ID_coast` (`ID_coast`),
  KEY `ID_status` (`ID_status`),
  KEY `ID_country` (`ID_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `info`
--


-- --------------------------------------------------------

--
-- Дублирующая структура для представления `lightinfo`
--
CREATE TABLE IF NOT EXISTS `lightinfo` (
`ID_info` int(11)
,`short_annotation` tinytext
,`ID_photo` int(11)
,`ID_realty` int(11)
,`ID_coast` int(11)
,`ID_status` int(11)
,`ID_country` int(11)
,`cost` decimal(10,2)
);
-- --------------------------------------------------------

--
-- Структура таблицы `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `ID_photo` int(11) NOT NULL auto_increment,
  `commentary` varchar(60) default NULL,
  `photo_name` varchar(30) default NULL,
  `big` varchar(30) default NULL,
  `small` varchar(30) default NULL,
  PRIMARY KEY  (`ID_photo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `photo`
--


-- --------------------------------------------------------

--
-- Структура таблицы `realty`
--

CREATE TABLE IF NOT EXISTS `realty` (
  `ID_realty` int(11) NOT NULL auto_increment,
  `realty_type` varchar(20) default NULL,
  `realty_type_defenition` char(2) default NULL,
  PRIMARY KEY  (`ID_realty`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `realty`
--

INSERT INTO `realty` (`ID_realty`, `realty_type`, `realty_type_defenition`) VALUES
(1, 'Apartment', 'AP'),
(2, 'House/villa', 'VL'),
(3, 'Townhouse', 'TH'),
(4, 'Investment projects', 'IN'),
(5, 'Land', 'LD'),
(6, 'Commercial realty', 'CM');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `short`
--
CREATE TABLE IF NOT EXISTS `short` (
`ID_info` int(11)
,`ID_offer` tinytext
,`short_annotation` tinytext
,`short_description` tinytext
,`ID_photo` int(11)
,`ID_realty` int(11)
,`ID_coast` int(11)
,`ID_status` int(11)
,`rental_description` tinytext
,`ID_country` int(11)
,`cost` decimal(10,2)
,`build_year` smallint(4)
,`full_area` decimal(10,2)
,`house_area` decimal(10,2)
,`terrace_area` decimal(10,2)
,`bedroom` varchar(100)
,`bathroom` varchar(100)
,`furniture` tinytext
,`swimming_pool` varchar(100)
,`fireplace` varchar(100)
,`distance_to_sea` decimal(10,2)
,`credit_per_month` decimal(10,2)
,`address` tinytext
,`location` tinytext
,`comment` text
,`full_description` text
,`coast_name` varchar(20)
,`small` varchar(30)
,`status_type` varchar(15)
,`realty_type` varchar(20)
);
-- --------------------------------------------------------

--
-- Структура таблицы `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `ID_status` int(11) NOT NULL auto_increment,
  `status_type` varchar(15) default NULL,
  PRIMARY KEY  (`ID_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `status`
--

INSERT INTO `status` (`ID_status`, `status_type`) VALUES
(1, 'rent'),
(2, 'sale'),
(3, 'sale/rent');

-- --------------------------------------------------------

--
-- Структура для представления `lightinfo`
--
DROP TABLE IF EXISTS `lightinfo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `catalogue_of_realty`.`lightinfo` AS select `catalogue_of_realty`.`info`.`ID_info` AS `ID_info`,`catalogue_of_realty`.`info`.`short_annotation` AS `short_annotation`,`catalogue_of_realty`.`info`.`ID_photo` AS `ID_photo`,`catalogue_of_realty`.`info`.`ID_realty` AS `ID_realty`,`catalogue_of_realty`.`info`.`ID_coast` AS `ID_coast`,`catalogue_of_realty`.`info`.`ID_status` AS `ID_status`,`catalogue_of_realty`.`info`.`ID_country` AS `ID_country`,`catalogue_of_realty`.`info`.`cost` AS `cost` from `catalogue_of_realty`.`info`;

-- --------------------------------------------------------

--
-- Структура для представления `short`
--
DROP TABLE IF EXISTS `short`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `catalogue_of_realty`.`short` AS select `catalogue_of_realty`.`info`.`ID_info` AS `ID_info`,`catalogue_of_realty`.`info`.`ID_offer` AS `ID_offer`,`catalogue_of_realty`.`info`.`short_annotation` AS `short_annotation`,`catalogue_of_realty`.`info`.`short_description` AS `short_description`,`catalogue_of_realty`.`info`.`ID_photo` AS `ID_photo`,`catalogue_of_realty`.`info`.`ID_realty` AS `ID_realty`,`catalogue_of_realty`.`info`.`ID_coast` AS `ID_coast`,`catalogue_of_realty`.`info`.`ID_status` AS `ID_status`,`catalogue_of_realty`.`info`.`rental_description` AS `rental_description`,`catalogue_of_realty`.`info`.`ID_country` AS `ID_country`,`catalogue_of_realty`.`info`.`cost` AS `cost`,`catalogue_of_realty`.`info`.`build_year` AS `build_year`,`catalogue_of_realty`.`info`.`full_area` AS `full_area`,`catalogue_of_realty`.`info`.`house_area` AS `house_area`,`catalogue_of_realty`.`info`.`terrace_area` AS `terrace_area`,`catalogue_of_realty`.`info`.`bedroom` AS `bedroom`,`catalogue_of_realty`.`info`.`bathroom` AS `bathroom`,`catalogue_of_realty`.`info`.`furniture` AS `furniture`,`catalogue_of_realty`.`info`.`swimming_pool` AS `swimming_pool`,`catalogue_of_realty`.`info`.`fireplace` AS `fireplace`,`catalogue_of_realty`.`info`.`distance_to_sea` AS `distance_to_sea`,`catalogue_of_realty`.`info`.`credit_per_month` AS `credit_per_month`,`catalogue_of_realty`.`info`.`address` AS `address`,`catalogue_of_realty`.`info`.`location` AS `location`,`catalogue_of_realty`.`info`.`comment` AS `comment`,`catalogue_of_realty`.`info`.`full_description` AS `full_description`,`catalogue_of_realty`.`coast`.`coast_name` AS `coast_name`,`catalogue_of_realty`.`photo`.`small` AS `small`,`catalogue_of_realty`.`status`.`status_type` AS `status_type`,`catalogue_of_realty`.`realty`.`realty_type` AS `realty_type` from ((((`catalogue_of_realty`.`info` join `catalogue_of_realty`.`coast`) join `catalogue_of_realty`.`photo`) join `catalogue_of_realty`.`realty`) join `catalogue_of_realty`.`status`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `info`
--
ALTER TABLE `info`
  ADD CONSTRAINT `info_ibfk_1` FOREIGN KEY (`ID_photo`) REFERENCES `photo` (`ID_photo`),
  ADD CONSTRAINT `info_ibfk_2` FOREIGN KEY (`ID_realty`) REFERENCES `realty` (`ID_realty`),
  ADD CONSTRAINT `info_ibfk_3` FOREIGN KEY (`ID_coast`) REFERENCES `coast` (`ID_coast`),
  ADD CONSTRAINT `info_ibfk_4` FOREIGN KEY (`ID_status`) REFERENCES `status` (`ID_status`),
  ADD CONSTRAINT `info_ibfk_5` FOREIGN KEY (`ID_country`) REFERENCES `country` (`ID_country`);
