<?php

class InfoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','output'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Info;
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
       
		if(isset($_POST['Info']))
		{   
            //random number for image
            $random = rand(0,99);
            //date of uploading image
            $date = DATE('Y_M_d_h_i_s');
            
			$model->attributes=$_POST['Info'];
            
            $photo_comment = $model->Commentary_to_photo;
            //get instance of image
            $upload_image = CUploadedFile::getInstance($model,'image');
            //setting the image name
            $file_name = "{$random}_{$date}.".$upload_image->extensionName; 
            //saving image to the folder
            $upload_image->saveAs('uploads/big/'.$file_name);
            //add extension for resize
            Yii::import('application.extensions.EWideImage.EWideImage');
            //resize and save image with new name(adding 'small' to the name)
            EWideImage::load('uploads/big/'.$file_name)->resize(140, 140)->saveToFile('uploads/small/'.'small_'.$file_name);
            //create new object of Photo
            $newPhoto = new Photo();
            //add to db the photo name which user gave
            $newPhoto->photo_name = $file_name;
            //add to db url of the big photo
            $newPhoto->big = 'uploads/big/'.$file_name;
            //add to db url of the small photo
            $newPhoto->small = 'uploads/small/'.'small_'.$file_name;
            //add to db commentary to the photo if user gave it
            $newPhoto->commentary = $photo_comment;
            //saving the data to db
            $newPhoto->save();
            //saving to table Info ID of the photo
            $model->ID_photo = $newPhoto->ID_photo;
            
			if($model->save()){
                $model->after();
				$this->redirect(array('view','id'=>$model->ID_info));
                
                
            }
        }

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Info']))
		{
			$model->attributes=$_POST['Info'];
         
			if($model->save())
				$this->redirect(array('view','id'=>$model->ID_info));
                
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Info');
        $data = $dataProvider->getData();
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
            'data'=>$data,
        ));
	}
    
        public function actionOutput()
    {
        $dataProvider=new CActiveDataProvider('Info');
        $data = $dataProvider->getData();
        $this->render('output',array(
            'dataProvider'=>$dataProvider,
            'data'=>$data,
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Info('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Info']))
			$model->attributes=$_GET['Info'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Info::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='info-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
