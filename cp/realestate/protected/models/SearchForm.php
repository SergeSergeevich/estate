<?php

/**
 * SearchForm class.
 * SearchForm is the data structure for keeping
 * search form data. It is used by the 'search' action of 'SiteController'.
 */
class SearchForm extends CFormModel
{
    public $textId;
    public $number;
    public $coastNumber;
    public $searchString;
   

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            //array('textId, number', 'required'),
            array('searchString', 'required'),
            // email has to be a valid email address
            //array('email', 'email'),
            // verifyCode needs to be entered correctly
            //array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'textId'  => 'Text Id',
            'number'  => 'Number',
            'coastNumber'=>'Coast Number',
        );
    }
    
    public function parse($str)
    {
        $result = array();
        
        $result[] = substr($str, 0, 2);
        
        if ($pos = strpos($str, '-')) {
            $result[] = substr($str, 2, ($pos-2));
            $result[] = substr($str, $pos+1);    
        } else if ($pos = strpos($str, '/')) {
            $result[] = substr($str, 2, ($pos-2));
            $result[] = substr($str, $pos+1);
        } else {
            $result[] = substr($str,2);
        }
        
        return $result;
    }
}