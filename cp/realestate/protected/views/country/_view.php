<?php
/* @var $this CountryController */
/* @var $data Country */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_country')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_country), array('view', 'id'=>$data->ID_country)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country_name')); ?>:</b>
	<?php echo CHtml::encode($data->country_name); ?>
	<br />


</div>