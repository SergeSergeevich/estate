<?php
/* @var $this ShortController */
/* @var $model Short */

$this->breadcrumbs=array(
	'Shorts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Short', 'url'=>array('index')),
	array('label'=>'Create Short', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#short-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Shorts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'short-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ID_info',
		'ID_offer',
		'short_annotation',
		'short_description',
		'ID_photo',
		'ID_realty',
		/*
		'ID_coast',
		'ID_status',
		'rental_description',
		'ID_country',
		'cost',
		'build_year',
		'full_area',
		'house_area',
		'terrace_area',
		'bedroom',
		'bathroom',
		'furniture',
		'swimming_pool',
		'fireplace',
		'distance_to_sea',
		'credit_per_month',
		'address',
		'location',
		'comment',
		'full_description',
		'coast_name',
		'small',
		'status_type',
		'realty_type',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
