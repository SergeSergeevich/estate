<?php
/* @var $this ShortController */
/* @var $model Short */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_info'); ?>
		<?php echo $form->textField($model,'ID_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_offer'); ?>
		<?php echo $form->textArea($model,'ID_offer',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'short_annotation'); ?>
		<?php echo $form->textArea($model,'short_annotation',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'short_description'); ?>
		<?php echo $form->textArea($model,'short_description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_photo'); ?>
		<?php echo $form->textField($model,'ID_photo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_realty'); ?>
		<?php echo $form->textField($model,'ID_realty'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_coast'); ?>
		<?php echo $form->textField($model,'ID_coast'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_status'); ?>
		<?php echo $form->textField($model,'ID_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rental_description'); ?>
		<?php echo $form->textArea($model,'rental_description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_country'); ?>
		<?php echo $form->textField($model,'ID_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cost'); ?>
		<?php echo $form->textField($model,'cost',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'build_year'); ?>
		<?php echo $form->textField($model,'build_year'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'full_area'); ?>
		<?php echo $form->textField($model,'full_area',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'house_area'); ?>
		<?php echo $form->textField($model,'house_area',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'terrace_area'); ?>
		<?php echo $form->textField($model,'terrace_area',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bedroom'); ?>
		<?php echo $form->textField($model,'bedroom',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bathroom'); ?>
		<?php echo $form->textField($model,'bathroom',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'furniture'); ?>
		<?php echo $form->textArea($model,'furniture',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'swimming_pool'); ?>
		<?php echo $form->textField($model,'swimming_pool',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fireplace'); ?>
		<?php echo $form->textField($model,'fireplace',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distance_to_sea'); ?>
		<?php echo $form->textField($model,'distance_to_sea',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'credit_per_month'); ?>
		<?php echo $form->textField($model,'credit_per_month',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'location'); ?>
		<?php echo $form->textArea($model,'location',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'full_description'); ?>
		<?php echo $form->textArea($model,'full_description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coast_name'); ?>
		<?php echo $form->textField($model,'coast_name',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'small'); ?>
		<?php echo $form->textField($model,'small',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_type'); ?>
		<?php echo $form->textField($model,'status_type',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realty_type'); ?>
		<?php echo $form->textField($model,'realty_type',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->