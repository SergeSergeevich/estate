<?php
/* @var $this ShortController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shorts',
);

$this->menu=array(
	array('label'=>'Create Short', 'url'=>array('create')),
	array('label'=>'Manage Short', 'url'=>array('admin')),
);
?>

<h1>Shorts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
