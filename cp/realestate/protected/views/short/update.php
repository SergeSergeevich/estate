<?php
/* @var $this ShortController */
/* @var $model Short */

$this->breadcrumbs=array(
	'Shorts'=>array('index'),
	$model->ID_info=>array('view','id'=>$model->ID_info),
	'Update',
);

$this->menu=array(
	array('label'=>'List Short', 'url'=>array('index')),
	array('label'=>'Create Short', 'url'=>array('create')),
	array('label'=>'View Short', 'url'=>array('view', 'id'=>$model->ID_info)),
	array('label'=>'Manage Short', 'url'=>array('admin')),
);
?>

<h1>Update Short <?php echo $model->ID_info; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>