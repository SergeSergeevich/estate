<?php
/* @var $this CoastController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Coasts',
);

$this->menu=array(
	array('label'=>'Create Coast', 'url'=>array('create')),
	array('label'=>'Manage Coast', 'url'=>array('admin')),
);
?>

<h1>Coasts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
