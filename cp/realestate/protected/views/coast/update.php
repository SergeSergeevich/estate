<?php
/* @var $this CoastController */
/* @var $model Coast */

$this->breadcrumbs=array(
	'Coasts'=>array('index'),
	$model->ID_coast=>array('view','id'=>$model->ID_coast),
	'Update',
);

$this->menu=array(
	array('label'=>'List Coast', 'url'=>array('index')),
	array('label'=>'Create Coast', 'url'=>array('create')),
	array('label'=>'View Coast', 'url'=>array('view', 'id'=>$model->ID_coast)),
	array('label'=>'Manage Coast', 'url'=>array('admin')),
);
?>

<h1>Update Coast <?php echo $model->ID_coast; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>