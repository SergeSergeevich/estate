<?php
/* @var $this CoastController */
/* @var $model Coast */

$this->breadcrumbs=array(
	'Coasts'=>array('index'),
	$model->ID_coast,
);

$this->menu=array(
	array('label'=>'List Coast', 'url'=>array('index')),
	array('label'=>'Create Coast', 'url'=>array('create')),
	array('label'=>'Update Coast', 'url'=>array('update', 'id'=>$model->ID_coast)),
	array('label'=>'Delete Coast', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_coast),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Coast', 'url'=>array('admin')),
);
?>

<h1>View Coast #<?php echo $model->ID_coast; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_coast',
		'coast_name',
	),
)); ?>
