<?php
/* @var $this CoastController */
/* @var $model Coast */

$this->breadcrumbs=array(
	'Coasts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Coast', 'url'=>array('index')),
	array('label'=>'Manage Coast', 'url'=>array('admin')),
);
?>

<h1>Create Coast</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>