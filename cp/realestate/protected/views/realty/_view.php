<?php
/* @var $this RealtyController */
/* @var $data Realty */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_realty')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_realty), array('view', 'id'=>$data->ID_realty)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realty_type')); ?>:</b>
	<?php echo CHtml::encode($data->realty_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realty_type_defenition')); ?>:</b>
	<?php echo CHtml::encode($data->realty_type_defenition); ?>
	<br />


</div>