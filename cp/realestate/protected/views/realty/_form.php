<?php
/* @var $this RealtyController */
/* @var $model Realty */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'realty-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'realty_type'); ?>
		<?php echo $form->textField($model,'realty_type',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'realty_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'realty_type_defenition'); ?>
		<?php echo $form->textField($model,'realty_type_defenition',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'realty_type_defenition'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->