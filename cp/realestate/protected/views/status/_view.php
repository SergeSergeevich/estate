<?php
/* @var $this StatusController */
/* @var $data Status */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_status')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_status), array('view', 'id'=>$data->ID_status)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_type')); ?>:</b>
	<?php echo CHtml::encode($data->status_type); ?>
	<br />


</div>