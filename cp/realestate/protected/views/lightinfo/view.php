<?php
/* @var $this LightinfoController */
/* @var $model Lightinfo */

$this->breadcrumbs=array(
	'Lightinfos'=>array('index'),
	$model->ID_info,
);

$this->menu=array(
	array('label'=>'List Lightinfo', 'url'=>array('index')),
	array('label'=>'Create Lightinfo', 'url'=>array('create')),
	array('label'=>'Update Lightinfo', 'url'=>array('update', 'id'=>$model->ID_info)),
	array('label'=>'Delete Lightinfo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_info),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Lightinfo', 'url'=>array('admin')),
);
?>

<h1>View Lightinfo #<?php echo $model->ID_info; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_info',
		'short_annotation',
		'ID_photo',
		'ID_realty',
		'ID_coast',
		'ID_status',
		'ID_country',
		'cost',
	),
)); ?>
