<?php
/* @var $this LightinfoController */
/* @var $model Lightinfo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lightinfo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_info'); ?>
		<?php echo $form->textField($model,'ID_info'); ?>
		<?php echo $form->error($model,'ID_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short_annotation'); ?>
		<?php echo $form->textArea($model,'short_annotation',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'short_annotation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_photo'); ?>
		<?php echo $form->textField($model,'ID_photo'); ?>
		<?php echo $form->error($model,'ID_photo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_realty'); ?>
		<?php echo $form->textField($model,'ID_realty'); ?>
		<?php echo $form->error($model,'ID_realty'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_coast'); ?>
		<?php echo $form->textField($model,'ID_coast'); ?>
		<?php echo $form->error($model,'ID_coast'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_status'); ?>
		<?php echo $form->textField($model,'ID_status'); ?>
		<?php echo $form->error($model,'ID_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_country'); ?>
		<?php echo $form->textField($model,'ID_country'); ?>
		<?php echo $form->error($model,'ID_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->