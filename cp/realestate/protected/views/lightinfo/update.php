<?php
/* @var $this LightinfoController */
/* @var $model Lightinfo */

$this->breadcrumbs=array(
	'Lightinfos'=>array('index'),
	$model->ID_info=>array('view','id'=>$model->ID_info),
	'Update',
);

$this->menu=array(
	array('label'=>'List Lightinfo', 'url'=>array('index')),
	array('label'=>'Create Lightinfo', 'url'=>array('create')),
	array('label'=>'View Lightinfo', 'url'=>array('view', 'id'=>$model->ID_info)),
	array('label'=>'Manage Lightinfo', 'url'=>array('admin')),
);
?>

<h1>Update Lightinfo <?php echo $model->ID_info; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>