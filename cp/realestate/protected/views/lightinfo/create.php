<?php
/* @var $this LightinfoController */
/* @var $model Lightinfo */

$this->breadcrumbs=array(
	'Lightinfos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Lightinfo', 'url'=>array('index')),
	array('label'=>'Manage Lightinfo', 'url'=>array('admin')),
);
?>

<h1>Create Lightinfo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>