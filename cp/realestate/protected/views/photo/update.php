<?php
/* @var $this PhotoController */
/* @var $model Photo */

$this->breadcrumbs=array(
	'Photos'=>array('index'),
	$model->ID_photo=>array('view','id'=>$model->ID_photo),
	'Update',
);

$this->menu=array(
	array('label'=>'List Photo', 'url'=>array('index')),
	array('label'=>'Create Photo', 'url'=>array('create')),
	array('label'=>'View Photo', 'url'=>array('view', 'id'=>$model->ID_photo)),
	array('label'=>'Manage Photo', 'url'=>array('admin')),
);
?>

<h1>Update Photo <?php echo $model->ID_photo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>