<?php
/* @var $this PhotoController */
/* @var $model Photo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'commentary'); ?>
		<?php echo $form->textField($model,'commentary',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'commentary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'photo_name'); ?>
		<?php echo $form->textField($model,'photo_name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'photo_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'big'); ?>
		<?php echo $form->textField($model,'big',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'big'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'small'); ?>
		<?php echo $form->textField($model,'small',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'small'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->